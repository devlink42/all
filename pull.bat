@echo off

echo Place this file in the parent folder of all your repo location

set /p "path=Names of folders that are git repo (you can put multiple 'regex' separated by a space): "

for /D %%i in (%path%) do (
    cd %%i
    git pull
    cd ..
)
